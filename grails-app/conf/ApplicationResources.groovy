//use the minified version in production
import static grails.util.GrailsUtil.*

def minified = isDevelopmentEnv() ? "" : ".min"

println "The grails development env is ${isDevelopmentEnv()}"


modules = {
    application {
        resource url:'js/application.js'
    }
	'jquery-ui'{
		resource url: "/js/jquery-ui-1.10.3.custom${minified}.js"
		resource url: "/css/ui-lightness/jquery-ui-1.10.3.custom${minified}.css"
	}
}