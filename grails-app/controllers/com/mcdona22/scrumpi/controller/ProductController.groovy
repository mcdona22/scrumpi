package com.mcdona22.scrumpi.controller

import com.mcdona22.scrumpi.domain.Product

class ProductController {

    def index() {
		redirect action: 'list'
	}
	
	def list(){
		def products = Product.list([sort:"name", order: "asc"])
		render view:"list", model:[products: products]
		
	}

	def edit(){
		render view: "edit", model:[]
	}
		
	def save(){
		log.info "save action -  params: $params"
		Product p = new Product(params)
		if( p.validate()){
			p.save()
			flash.message = "Successfully saved product '${p.name}'"
			redirect action:'list'
		} else {
			println "oh no man - validation errors"
			flash.message = "There have been errors validating this product"
			render view:"edit", model:[product:p]
		}		
	}
	
	def show(){
		if(!params.id){
			log.info "No product ID provided - showing list"
			redirect action: 'list'
			return
		}
		
		Product product = Product.get(params.id)
		if(! product){
			flash.message = "No product exists with an ID of $params.id"
			log.warn "Attempted to show product $params.id which does not exist"
			redirect action: 'list'
		}
		render view: 'show', model:[product: product, stories:[0:"Create A Story", 5:"Add tasks to a story", 12:"Allocate a story to a sprint", 98:"Create Sprints"]]
	}
	
	def getSomeStuff(){
		log.info "returning the content in an AJAXy way"
		render "This is where the sprints will go"
		
	}
}