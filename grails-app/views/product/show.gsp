<html>
<head>
<title>Backlog</title>
<meta name="layout" content="scrumpi" />
<g:javascript library="jquery" />
<%--<r:require module="jquery-ui" />--%>
<r:layoutResources />

<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<style>
#sortable {
	list-style-type: none;
	margin: 0;
	padding: 0;
	width: 100%;
}

#sortable li {
	margin: 0 3px 3px 3px;
	padding: 0.4em;
	padding-left: 1.5em;
	font-size: 1.4em;
	height: 18px;
}

#sortable li span {
	position: absolute;
	margin-left: -1.3em;
}
</style>
<script>
	$(function() {
		$("#sortable").sortable();
		$("#sortable").disableSelection();
	});
</script>

</head>
<body>
	<h3>Product: ${product.name }(${product.prefix})</h3>
	<div id="backlog" class="column-left">
		<h4>Uncommitted Backlog</h4>
		<ul id="sortable">
			<g:each in="${stories}" var="story">

				<li class="ui-state-default"><span
					class="ui-icon ui-icon-arrowthick-2-n-s"></span>(id:${story.key}) ${story.value}</li>
			</g:each>
		</ul>
	</div>
	<div id="sprints" class="column-left">
	<h4>Sprints</h4>

	<div id="stuff">
	</div>

</div>
<script>	
	onload="${remoteFunction(action: 'stuff',
    update: [success: 'stuff'])}">
</script>

         
</body>
</html>

