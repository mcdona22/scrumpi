<html>
<head>
	<title>Editing Product</title>
	<meta name="layout" content="scrumpi" />
</head>
<body>

<h2>Editing Product</h2>
<g:if test="${product?.errors}" >
<div class="error-details">
${flash.message}

	<g:renderErrors bean="${product }" />
</g:if>
</div>
<fieldset>

<g:form name="productEdit" url="[controller:'product',action:'save']">
	<g:hiddenField name="id" value="${product?.id }" />
	<p><label for="name">Name</label> <g:textField name="name" value="${product?.name}" /> <br /></p>
	<p><label for="name">Prefix</label>  <g:textField name="prefix" value="${product?.prefix}" /> <br /></p>
	<p><label for="name">Description</label>  <g:textField name="description" value="${product?.description}" /><br /></p>
	<br/>
	
	<p> <input type="submit" value="Save" class="formbutton"/> </p>
	
</g:form>
</fieldset>



</body>
</html>
	
	