<html>
<head>
	<title>Products List</title>
	<meta name="layout" content="scrumpi" />
</head>
	<h2>Products</h2>
	<g:if test="${flash.message }" >
		<div class="message">
		${flash.message }
		</div>
	</g:if>
	
	
	<table>
	<tr>
		<th>ID</th><th>Prefix</th><th>Name</th><th>Description</th>
	</tr>
		
	<g:each in="${products}" var="p">
		<tr>
			<td><g:link action="show" id="${p.id}">show ${p.prefix}</g:link>
			</td>
			<td>${p.prefix}</td>
			<td>${p.name}</td><td>${p.description}</td>
			</tr>	
	</g:each>
	
	</table>
	
	<g:link action="edit" >Create New Product</g:link>
</html>