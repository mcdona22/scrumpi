<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="scrumpi"/>
		<title>Welcome</title>
	</head>
	<body>
	<h2>Welcome to Scrumpi!</h2>
	This is Scrumpi - a simple agile story management tool developed by John Mac.  At its heart are some simple principles:
	<ul>
		<li>It will not attempt to tell you how to run your variant of Agile</li>
		<li>It will model the backlog metaphor as simply as possible</li>
		<li>Thats all!</li>
	</ul>
	</body>
</html>
