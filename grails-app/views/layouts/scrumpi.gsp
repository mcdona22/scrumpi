<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Scrumpi
			<g:layoutTitle default=""/>
		</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'styles.css')}" type="text/css">
		<g:layoutHead/>
		<r:layoutResources />
	</head>

</head>

<body>


	<div id="wrapper"><%--
	
		<div id="sitename">
			<h1><a href="#">Scrumpi</a></h1>
		</div>
		
		--%><div id="nav">
			<ul class="clear">
			
				<!-- MENU -->
				<li><a href="${createLink(uri: '/')}"><span>Home</span></a></li>
				<%--
				<li class="current"><a href="examples.html"><span>Examples</span></a></li>
				--%>
				<li class="current"><g:link controller="product" action="index"><span>Products</span></g:link></li>
				<li><a href="#"><span>Admin</span></a></li>
				<!-- END MENU -->
				
			</ul>
		</div>
		<div id="header">
			<h2>Scrumpi: Agile Story Management Tool</h2>
			<div class="tagline">Make it work, make it right, make it fast!</div>
		</div>
		<div id="body" class="clear">
			<g:layoutBody/>
		</div>
		<div id="footer" class="clear">
			<p class="left">&copy; 2013 Scrumpi.</p>
			<p class="right">Application developed by John Mac</p>
		</div>
	</div>
	
			<r:layoutResources />
</body>
</html>