package com.mcdona22.scrumpi.domain

class StoryContainer {
	String name
	List priority = []
	
	static hasMany = [stories: Story]
	static belongsTo = [product: Product]
	
	static constraints = {
		product nullable: true
	}
	
	String toString(){ "{id: $id, name: $name, priority: $priority, product: $product}" }
		
	def addStory(Story story, int priorityPosition = 0){
		if (story.id == null){
			this.save(flush:true)
			story.save()	// we must have an id to record
		}
		this.addToStories story
		priorityPosition > priority.size() - 1 ? priority << story.id 
		 	: priority.add(priorityPosition, story.id)
		story.container = this	
	}
}
