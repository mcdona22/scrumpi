package com.mcdona22.scrumpi.domain

class Sprint {
	String name
	Date start
	Date finish

    static constraints = {
    }
	
	String toString(){ "{name: $name, start: $start, finish: $finish}" }
}
