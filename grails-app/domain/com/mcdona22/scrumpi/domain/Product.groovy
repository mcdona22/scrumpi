package com.mcdona22.scrumpi.domain

class Product {
	String name
	String prefix
	String description
	StoryContainer backlog = new StoryContainer (name: "Uncommitted Backlog")

	static hasMany = [containers : StoryContainer]
	
    static constraints = {
		name blank:false, maxSize:40
		prefix blank: false, maxSize: 6, unique: true
		description blank:true, nullable:true
    }
	
	String toString(){ "{id: $id, prefix: '$prefix', name: '$name', description: '$description'}" }
}
