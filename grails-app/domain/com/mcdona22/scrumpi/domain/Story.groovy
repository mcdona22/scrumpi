package com.mcdona22.scrumpi.domain

class Story {
	static belongsTo = [container : StoryContainer]
	String name
	String desc
	String sponsor
	int points
	
		
    static constraints = {
		name blank:false, maxSize: 30
		desc nullable: true
		sponsor nullable: true
		points  validator: {val, obj -> val == null || val >= 0}
		container nullable: true
    }
		
	String toString(){ "{id:$id, name: '$name', points: ${points?:null}, container: $container"}
	
}
