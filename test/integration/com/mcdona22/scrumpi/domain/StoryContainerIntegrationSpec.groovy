package com.mcdona22.scrumpi.domain

import grails.plugin.spock.IntegrationSpec

class StoryContainerIntegrationSpec extends IntegrationSpec {
	private def StoryContainer container
	def name = 'Test Container'
	
	def setup() {
		container = new StoryContainer( name: name)
	}

	def cleanup() {
	}

	def "should persist a simple container"(){
		when:
			container.save()
			StoryContainer c = StoryContainer.findById(container.id)
		then:
			assert c.name == name
			println c
	}
	
	def "should add a story to a container with no existing stories"(){
		setup: Story s = new Story (name: 'test-story', points: 5)
		when: 
			container.addStory(s)
			container.save(flush:true)
		then:
			assert container.priority.size() == 1
			assert container.priority[0] == s.id
			assert s.container == container
			println container
			println s
	}

	def "should add a story to a container even if the story is not yet saved"(){
		setup: Story s = new Story (name: 'unsaved test-story', points: 2)
		when: container.addStory(s)
		then:
			assert container.priority.size() == 1
			assert container.priority[0] != null
			assert s.container == container
			println container
			println s
	}
	
	def "should add multiple stories to the default position"(){
		setup:
			def storyCount = 5
			List storyIds = []
			storyCount.times{
				Story s = new Story(name: "test story $it", points : it)
				container.addStory(s)
				storyIds.add(0, s.id)
			}
		expect:
			container.priority.size() == storyCount
			int i = 0
			assert allStoriesHaveCorrectContainerAssigned(container)
			container.priority.each{ id ->
				assert id == storyIds[i++]
			}
	}
	
	def "should add a story at a specific point in the priority"(){
		setup:
			def existingStories = 12
			def targetIndex = 3
			existingStories.times{
				Story s = new Story(name: "test story $it", points : 2)
				container.addStory(s)
			}
			Story story = new Story(name: "insert in the middle story", points : 7)
		when: container.addStory(story, targetIndex)
		then:
			assert container.priority.size() == existingStories + 1
			assert container.priority[targetIndex] == story.id
			assert allStoriesHaveCorrectContainerAssigned(container)
	}
	
	def "should add a story to the back of the priority if the index is beyond the boundary for safety"(){
		setup:
			def storiesToAdd = 4
			createSomeStoriesAndAddToContainer(storiesToAdd, container)
			Story story = new Story (name: "add beyond range", points : 3)
		when: container.addStory(story, storiesToAdd + 3)
		then:
			container.priority.size() == storiesToAdd + 1
			container.priority[storiesToAdd] == story.id	// we have added an extra 1
			assert allStoriesHaveCorrectContainerAssigned(container)
	}
	
	def "saving a container should also save any of its stories"(){
		setup:
			def storiesToCreate = 12
			createAndAddStoriesToContainer container, storiesToCreate
			container.save(flush:true)
		when: StoryContainer c = StoryContainer.get(container.id)
		then:
			assert c.name == name
			assert c.priority.size() == storiesToCreate
			c.priority.each{ storyId ->
				assert Story.get(storyId).container == c
			}
	}
	
	def "all the containers stories should be available from its stories list"(){
		setup:
			def storyCount = 5
			createAndAddStoriesToContainer container, storyCount
			container.save(flush:true)
		when: StoryContainer c = StoryContainer.get(container.id)
		then:
			assert c.stories.size() == storyCount	
			c.stories.each{ story ->
				assert story.container == c
			}
	}
	
	def "when saved the priority should contain only IDs relating to associated stories"(){
	setup:
		def storyCount = 12
		createAndAddStoriesToContainer container, storyCount
		container.save(flush:true)
	when: StoryContainer c = StoryContainer.get(container.id)
	then:
		assert c.priority.size() == c.stories.size()
		c.stories.each {story ->
			assert c.priority.contains(story.id)
		}
	}
	
	// Helpers
	
	void createAndAddStoriesToContainer(StoryContainer sc, int storyCount){
		storyCount.times{ i ->
			Story s = new Story(name:"story $i", points: 4, desc :"test story $i")
			sc.addStory s
		}
	}
	
	private void createSomeStoriesAndAddToContainer(int count, StoryContainer c, String title = 'Test Stories'){
		count.times{
			c.addStory(new Story(name: "$title-$it"))
		}
	}

	private boolean allStoriesHaveCorrectContainerAssigned(StoryContainer c){
		c.priority.each{ id ->
			assert Story.findById(id).container == c
		}
	}
}