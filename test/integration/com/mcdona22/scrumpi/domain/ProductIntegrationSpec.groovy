package com.mcdona22.scrumpi.domain

import grails.plugin.spock.IntegrationSpec
import spock.lang.Ignore

class ProductIntegrationSpec extends IntegrationSpec {

	def setup() {
	}

	def cleanup() {
	}

	//@Ignore
	def "should save correctly to the database"(){
		setup:
			def prodName = "product name"
			def prodDesc = "product description"
			def prodPrefix = "prefix"
			Product product = new Product(name: prodName, description: prodDesc, prefix: prodPrefix)
			println product.backlog
			
		when:
			//product.backlog.save(flush:true)
			product.save(flush:true)
			Product p = Product.findById(product.id)
		then:
			assert p.name == prodName
			assert p.prefix == prodPrefix
			assert p.description == prodDesc
			assert p.backlog != null
			assert p.backlog.id != null
			assert p.backlog.priority.size() == 0	
			assert p.backlog.product != null
			println p
			println p.backlog
	}
	
}