package com.mcdona22.scrumpi.domain

import grails.test.mixin.TestFor
import spock.lang.Specification
import grails.test.mixin.Mock
import grails.test.mixin.TestMixin
import grails.test.mixin.domain.DomainClassUnitTestMixin
import org.junit.Ignore

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(StoryContainer)
@Mock(Story)
class StoryContainerSpec extends Specification {
	private StoryContainer container
	String containerName = "backlog"
	
	def setup() {
		mockDomain Story
		mockDomain StoryContainer
		container = new StoryContainer(name: containerName).save()
		container.save()
	}

	def "should construct correctly when using default contructor"(){
		given: container = new StoryContainer()
		expect:
			assert container.priority != null
			//assert container.workItem != null
	}
	
	def "should contruct correctly when using the map constructor"(){
		expect:
			assert container.priority != null
			assert container.name == containerName
	}
	
	

	
	
	

	// Helper methods ---------------------
		

		

}