package com.mcdona22.scrumpi.domain

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Product)
class ProductSpec extends Specification {
	String prodName = "Product Name"
	String prodDesc = "product description"
	String prodPrfx = "PROD-A"
	boolean outcome

	Product p

	def setup(){
		p = new Product(name:prodName, description: prodDesc, prefix: prodPrfx)
		mockForConstraintsTests(Product, [p])
		
	}

	def "should be able to create product with map constructor"(){
		expect:
		assert p.prefix == prodPrfx
		assert p.name == prodName
		assert p.description == prodDesc
		assert p.validate()
	}
	
	def "should validate name"(){
		when: p.name = testName
		then: p.validate() == outcome
		where:
			outcome|testName
			false| null
			false| "   "
			false|"Too Large ----------------------------------------------"
			true|"Decent Product Name"
	}
	
	def "should validate prefix"(){
		when:
			p.prefix = testPrefix
		then:
			p.validate() == outcome
		where:
			testPrefix| outcome
			" "| false			// no blank space
			null| false			// no nulls
			"PROD-Ax"| false	// too large
			"PROD-A"|true		// just right
	}
	
	def "should validate description correctly"(){
		when: p.description = testDescription
		then: p.validate() == outcome	
		where:
		outcome|testDescription
		true| null
		true| " "
		true| "some random description - there is no upper limit imposed"
	}

	def "should not permit duplicate prefixes"(){
		setup:
			p.save()
			Product product = new Product(name: "new Prod", prefix: p.prefix)
			println p
			println product
		expect:
			!product.validate()
	}	

	def "should create a product with a single uncommitted backlog"(){
		expect:
			assert p.backlog != null
			assert p.backlog.name == "Uncommitted Backlog"
			assert p.backlog.priority.size() == 0
	}
}