package com.mcdona22.scrumpi.domain

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Sprint)
class SprintSpec extends Specification {

	def setup() {
	}

	def cleanup() {
	}

	void "should create with map arg constructor"() {
		setup:
			String name = "sprint name"
			Date startDate = new Date()
			Date endDate = startDate + 7
		when: 
			Sprint sprint = new Sprint(name : name, start : startDate, finish: endDate)
		then:
			println sprint
			assert sprint.name == name
			assert sprint.start == startDate
			assert sprint.finish == endDate
		
	}
}