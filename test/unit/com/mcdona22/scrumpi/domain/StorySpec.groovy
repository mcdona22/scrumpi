package com.mcdona22.scrumpi.domain

import grails.test.mixin.TestFor
import spock.lang.Specification
import grails.test.mixin.Mock


@TestFor(Story)
@Mock(StoryContainer)
import grails.test.mixin.Mock

class StorySpec extends Specification{
	StoryContainer container
	Story story
	String name = "story name"
	int points = 1
	String narrative = "narrative gans here"
	String sponsor = "story sponsor"
	
	def setup(){
		container = new StoryContainer(name: "test-container")
		story = new Story(name:name, desc:narrative, points: points, sponsor:sponsor, container :container)
		mockForConstraintsTests(Story, [story])
	}

    def "should permit simple construction using map"(){
		expect:
			assert container == story.container
			assert name == story.name
			assert narrative == story.desc
			assert sponsor == story.sponsor	
			assert points == story.points
			
	}
	
	def "should validate the minimal story"(){
		setup: 
			Story workItem = new Story(name: "Story XYX", container: container)
			mockForConstraintsTests(Story, [workItem])	
		expect: 
			assert workItem.validate()		
	}
	
	def "should fail validation if the story size is less than zero"(){
		when: story.points = -1
		then: assert ! story.validate()
		
	}
	
	def "should fail validation if name is null"(){
		when: story.name = null
		then: assert ! story.validate()		
	}
	
	def "should fail validation if name is whitespace"(){
		when: story.name = "   "
		then: assert !story.validate()
	}
	
	def "should fail validation if name is too big"(){
		setup:
			String name = ""
			35.times{ name += it }
		when: story.name = name
		then: 
			assert !story.validate()
			assert "maxSize" == story.errors["name"]
	} 
	
	def "Should persist valid story"(){
		when: story.save(flush:true)
		then:
			assert 1 == Story.count()
			assert story.id != null
	}
	 
}
