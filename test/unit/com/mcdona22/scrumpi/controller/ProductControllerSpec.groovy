package com.mcdona22.scrumpi.controller

import org.apache.tools.ant.types.Description;

import grails.test.mixin.TestFor
import spock.lang.Specification
import grails.test.mixin.Mock
import grails.test.mixin.TestMixin
import grails.test.mixin.domain.DomainClassUnitTestMixin

import com.mcdona22.scrumpi.domain.Product


@TestFor(ProductController)
@Mock([Product])
@TestMixin(DomainClassUnitTestMixin)
class ProductControllerSpec extends Specification {


	void "index action should redirect to list action"() {
		when: controller.index()
		then: assert response.redirectUrl == "/product/list"
	}
	
	void "behaviour when no products are available"(){
		setup: mockDomain(Product)	// and its empty
		when: 
			controller.list()			
		then:
			assert model?.products == []
			assert view == "/product/list"			
	}
	
	void "list should behave correctly when single product is available"(){
		setup:
			def prodName = "product"
			def prefix = "PREFIX"

		mockDomain(Product, [
				[ name:prodName, prefix: prefix]
			])
		when:
			controller.list()
		then:
			assert model?.products.size == 1
			assert model.products[0].name == prodName
			assert model.products[0].prefix == prefix
			assert view == "/product/list"
	}
	
	void "list should behave correctly when multiples product are available"(){
		setup:
			def productNames = ["Product B", "Product A", "Product C"] //deliberately our of order
			def prefix = "CODE"
			int suffix = 0
			List productList = []
			productNames.each{ productName ->
				productList << [name: productName, prefix: "$prefix-${suffix++}"]
			}
			mockDomain(Product, productList)
			def sortedProductNames = productNames.sort()
			
		when: controller.list()
		then:
			assert view == "/product/list"		
			println model.products
			println sortedProductNames
			assert model.products.size() == productNames.size()
			productNames.size().times{i ->
				println "comparing ${sortedProductNames[i]} and  ${model.products[i].name}"
				assert sortedProductNames[i] == model.products[i].name
			}
	}
	
	void "should save correctly happy path"(){
		setup:
			int prodCount = Product.list().size()
			def testName = "Product Name"
			def testDesc = "Product Desc"
			def testPrefix = "PRFX"
			params.name = testName
			params.description = testDesc
			params.prefix = testPrefix
		when: controller.save()
		then:
			assert flash.message == "Successfully saved product '$testName'" 
			assert Product.list().size() == prodCount + 1
			assert Product.findByPrefix(testPrefix) != null
			assert response.redirectUrl == "/product/list"
	}
	
	void "should not attempt to save if mandatory fields are missing" (){
		
	}
	
	void "should save if description is missing"(){}
	
	void "should not attempt to save if a duplicate prefix is being submitted"(){
		setup:
			def name = "product X"
			def prefix = "PRFX-Z"
			def desc = "Test product for duplication purposes"
			new Product(name: "iintial product", prefix: prefix).save()
			println Product.findAll()
			params.name = name
			params.prefix = prefix
			params.description = desc
		when: controller.save()
		then: 
			assert flash.message == "There have been errors validating this product"
			assert Product.findAllByPrefix(prefix).size() == 1
			assert view == "/product/edit"
			assert model.product != null
	}

	void "should display the product list if show is called without parameter"(){
		when: controller.show()
		then: response.redirectUrl == "/product/list"
	}	
	
	void "should display the product list if show is called with invalid product id"(){
		setup: 
			def testId = 9999999
			params.id = testId	// doesnt exist
		when: controller.show()
		then: 
			assert response.redirectUrl == "/product/list"
			assert flash.message == "No product exists with an ID of $testId"
	}
	
	void "should show the product correctly when called with valid product id"(){
		setup:
			Product p = new Product(name:'Test Product', prefix: "TEST_P", description: "product description").save()
			params.id = p.id
		when: controller.show()
		then:
			assert model.product != null
			assert model.product?.id == p.id
			assert view == "/product/show"			
	}
}