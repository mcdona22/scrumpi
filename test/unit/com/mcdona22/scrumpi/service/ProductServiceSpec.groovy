package com.mcdona22.scrumpi.service

import com.mcdona22.scrumpi.domain.Product
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(ProductService)
class ProductServiceSpec extends Specification {
	ProductService service
	String prodName = "Product Name"
	String prodDesc = "product description"
	String prodPrfx = "PROD-A"
	
	def setup() {
		service = new ProductService()
	}

	def cleanup() {
	}


}
